import axios from "axios";

export interface IPost {
    id: string;
    title: string;
    description: string;
    date: string;
}

export const PostsRepository = {
    GetPosts: () => axios.get<IPost[]>("http://localhost:3001/posts"),
    AddPost: (post: IPost) => axios.post<IPost>("http://localhost:3001/posts", post),
    GetPostById: (id: string) => axios.get<IPost>(`http://localhost:3001/posts/${id}`),
    UpdatePost: (post: IPost) => axios.put<IPost>(`http://localhost:3001/posts/${post.id}`, post),	
}