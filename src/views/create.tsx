import { PostForm } from "../components/PostForm"

const CreatePage = () => {
    return (
        <div>
            <h1 className="text-2xl font-bold mb-4">Create a post</h1>
            <PostForm></PostForm>
        </div>
    )
}

export default CreatePage