import { useEffect, useState } from 'react'
import { IPost } from '../repositories/posts.repository'
import { PostsService }from '../services/posts.service'
import { PostComponent } from '../components/PostComponent'

export const IndexPage = () => {
    const [posts, setPosts] = useState<IPost[]>([])
    const [totalPosts, setTotalPosts] = useState<number>(0)

    const hasPosts = totalPosts > 0

    const getPosts = async () => {
        const response = await PostsService.GetPosts()
        
        if(response) { 
            setPosts(response)
            setTotalPosts(response.length)
        }
    }
    
    useEffect(() => {
        getPosts()
    }, [])

    return (
        <div>
            { hasPosts ?
                <div>{ posts.map((post) => <PostComponent key={post.id} post={post}></PostComponent>) }</div>
                :
                <div>No posts found</div>
            }
        </div>
    )
}