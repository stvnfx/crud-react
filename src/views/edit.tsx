import { useEffect, useState } from "react"
import { IPost } from "../repositories/posts.repository"
import { PostsService } from "../services/posts.service"
import { PostForm } from "../components/PostForm"

export const EditPage = () => {
    const [post, setPost] = useState<IPost | undefined>(undefined)

    const getPostById = async () => {
        const id = window.location.pathname.split('/')[2]

        const response = await PostsService.GetPostById(id)
        setPost(response)
    }

    useEffect(() => {
        getPostById()
    }, [])

    return (
        <div>
            <h1 className="text-2xl font-bold mb-4">Edit Page</h1>
            <PostForm post={post} rows={8}></PostForm>
        </div>
    )
}