import { IPost, PostsRepository } from "../repositories/posts.repository";

export const PostsService = {
    GetPosts: async () => {
        try {
            const { data } = await PostsRepository.GetPosts();

            return data
        } catch (error) {
            console.log(error)
        }
    },
    AddPost: async (post: IPost) => {
        try {
            const { data } = await PostsRepository.AddPost(post);

            return data
        } catch (error) {
            console.log(error)
        }
    },
    GetPostById: async (id: string) => {
        try {
            const { data } = await PostsRepository.GetPostById(id);

            return data
        } catch (error) {
            console.log(error)
        }
    },
    UpdatePost: async (post: IPost) => {
        try {
            const { data } = await PostsRepository.UpdatePost(post);

            return data
        } catch (error) {
            console.log(error)
        }
    },
}