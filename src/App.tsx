import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
import { IndexPage } from "./views/index";
import CreatePage from "./views/create";
import { EditPage } from "./views/edit";
import { NavigationComponent } from "./components/NavigationComponent";

const App = () => {
  return (
    <Router>
      <main className="container flex h-screen w-screen">
        <NavigationComponent/>
        <div className="w-3/4 p-8">
          <Routes>
            <Route path='/' element={<IndexPage/>}/>
            <Route path="/create-post" element={<CreatePage/>}/>
            <Route path="/edit-post/:id" element={<EditPage/>}/>
          </Routes>
        </div>
      </main>
    </Router>
  )
}

export default App
