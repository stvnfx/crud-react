import { IPost } from "../repositories/posts.repository"
import { format } from "date-fns"

export const PostComponent = ({post}: {post: IPost}) => {
    const formattedDate = format(new Date(post.date), 'dd-MM-yyyy')

    return (
        <div className="mb-8">
            <h2 className="text-xl font-bold mb-2">
                {post.title}
            </h2>
            <h3 className="text-sm text-slate-500 font-semibold">
                {formattedDate}
            </h3>
            <p className="mb-6">
                {post.description}
            </p>
            <a
                href={`/edit-post/${post.id}`}
                className="bg-green-500 hover:bg-green-700 text-white rounded-md transition-colors py-2 px-4"
            >
                Edit post
            </a>
        </div>
    )
}