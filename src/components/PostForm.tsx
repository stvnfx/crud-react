import { SyntheticEvent, useEffect, useState } from "react";
import { v4 as uuidv4 } from 'uuid';
import { IPost } from "../repositories/posts.repository";
import { PostsService } from "../services/posts.service";
import { Alert } from "./Alert";

interface IPostFormProps {
    post?: IPost;
    rows?: number;
}

export const PostForm = ({post, rows = 3}: IPostFormProps) => {
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [postIsAdded, setPostIsAdded] = useState(false);
    let alert;

    const handleSubmit = (event: SyntheticEvent) => {
        event.preventDefault();

        try {
            PostsService.AddPost({
                id: uuidv4(),
                title: title,
                description: description,
                date: new Date().toISOString()
            });

            setPostIsAdded(true);
            setTitle('');
            setDescription('');

            setTimeout(() => {
                window.location.href = '/';
            }, 1000);
        } catch (error) {
            console.log(error);
        }
    }

    const handleUpdate = (event: SyntheticEvent) => {
        event.preventDefault();

        if(post) {
            try {
                PostsService.UpdatePost({
                    id: post.id,
                    title: title,
                    description: description,
                    date: post.date
                });
    
                setPostIsAdded(true);
                setTitle('');
                setDescription('');
    
                setTimeout(() => {
                    window.location.href = '/';
                }, 1000);
            } catch (error) {
                console.log(error);
            }
        }
    }

    const handleTitleChange = (event) => {
        setTitle(event.target.value);
    }

    const handleDescriptionChange = (event) => {
        setDescription(event.target.value);
    }

    useEffect(() => {
        if(post) {
            setTitle(post.title);
            setDescription(post.description);
        }
    }, [post]);
    
    if(postIsAdded) {
        alert = <Alert message={post ? "Post is updated" : "Post is added"}/>;
    }

    return (
        <form
            className="w-1/2"
            onSubmit={post ? handleUpdate : handleSubmit}
        >
            {alert}
            <div className="flex flex-col mb-3">
                <label 
                    htmlFor="post-title"
                    className="font-bold"
                >
                    Title
                </label>
                <input
                    id="post-title"
                    value={title}
                    type="text"
                    className="border rounded-md p-2"
                    onChange={handleTitleChange}
                />
            </div>
            <div className="flex flex-col mb-3">
                <label
                    htmlFor="post-description"
                    className="font-bold"
                >
                    Description
                </label>
                <textarea
                    id="post-description"
                    value={description}
                    className="border rounded-md p-2"
                    rows={rows}
                    onChange={handleDescriptionChange}
                />
            </div>
            <button
                type="submit"
                className="bg-green-500 hover:bg-green-700 text-white rounded-md transition-colors py-2 px-4"
            >
                {post ? 'Edit post' : 'Add post'}
            </button>
        </form>
    )
}
