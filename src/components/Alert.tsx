
export const Alert = ({message}: {message: string}) => {
    return (
        <div className="border border-green-800 rounded-md p-4" role="alert">
            {message}
        </div>
    )
}