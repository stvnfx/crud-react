import { NavLink } from "react-router-dom";
import { NavigationLinks } from "../helpers/navigation";

export const NavigationComponent = () => {
    const navigationItems = NavigationLinks.map((link, index) => <NavLink to={link.path} key={index} className="block">{link.name}</NavLink>)

    return (
        <nav className="w-1/4 p-8 bg-slate-100 h-full">
            {navigationItems}
        </nav>
    )
}