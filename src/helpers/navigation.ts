export interface INavigationLink {
    name: string;
    path: string;
}

export const NavigationLinks: INavigationLink[] = [
    {name: "Dashboard", path: "/"}, {name: "Create Post", path: "/create-post"}
]